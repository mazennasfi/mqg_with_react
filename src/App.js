import MQU from "./MQU";
import React, { useState, useEffect } from "react";

function App() {
  const [products, setProducts] = useState([]);

  useEffect(
    () =>
      MQU.init({
        url: "http://localhost:8080/OLAP",
      }).then((MQG) => {
        MQG.QueryView()
          .attribute("department", "product.department.label")
          .attribute("city", "location.city.label")
          .attribute("month", "calendar.month.label")
          .measure("sales", "units", "TOTAL")
          .measure("profit", "TOTAL") //retail price
          .measure("returns", "TOTAL") // number of returned unites
          .groupBy({
            product: "department",
            location: "city",
            calendar: "month",
          }) //group the sales units by department
          .get()
          .then(function (data) {
            setProducts(data);
          });
      }),
    []
  );
  let measures = [];
  measures = products.map((one) => {
    return (
      <tr>
        <td> {one.department} </td>
        <td>{one.city}</td>
        <td> {one.month}</td>
        <td>{one.sales}</td>
        <td>{one.profit}</td>
        <td>{one.returns}</td>
      </tr>
    );
  });
  return (
    <div className="App">
      <table>
        <thead>
          <tr>
            <th>Department</th>
            <th>City</th>
            <th>Month</th>
            <th>Sales</th>
            <th>Profit</th>
            <th>Retuns</th>
          </tr>
        </thead>
        <tbody>{measures}</tbody>
      </table>
    </div>
  );
}

export default App;
